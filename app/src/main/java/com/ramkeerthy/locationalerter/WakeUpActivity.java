package com.ramkeerthy.locationalerter;

import androidx.appcompat.app.AppCompatActivity;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;

import static com.ramkeerthy.locationalerter.MainActivity.isStartLocationButtonEnabled;

public class WakeUpActivity extends AppCompatActivity {

    private static final String TAG = "WakeUpActivity";

    private SeekBar alarmDismissSeekBar;
    private AdView mAdView;
    private TextView alertTextView;

    int seekBarProgress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_wake_up);

        // Log.i(TAG, "onCreate: ");

        mAdView = findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        alertTextView = findViewById(R.id.alert_textview);
        String radius = String.valueOf(CoordinatesResolver.radius);
        String alertText = getResources().getText(R.string.your_destination)
                + " " + CoordinatesResolver.destinationName
                + " " + getResources().getText(R.string.is)
                + " " + radius
                + " " + getResources().getText(R.string.away);

        alertTextView.setText(alertText);

        alarmDismissSeekBar = findViewById(R.id.seekBar);
        alarmDismissSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarProgress = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(seekBarProgress > 60) {
                    seekBar.setProgress(100);

                    if(AlarmBroadcastReceiver.mp != null) {
                        AlarmBroadcastReceiver.mp.stop();
                    }

                    if(AlarmBroadcastReceiver.vibrator != null) {
                        AlarmBroadcastReceiver.vibrator.cancel();
                    }

                    Intent serviceIntent = new Intent(WakeUpActivity.this, LocationAlertService.class);
                    stopService(serviceIntent);

                    NotificationManager notificationManager = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        notificationManager = getSystemService(NotificationManager.class);
                        notificationManager.cancel(AlarmBroadcastReceiver.ALARM_NOTIFICATION_ID);
                    }

                    isStartLocationButtonEnabled = true;

                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                        // lock the screen
                    }

                    finish();
                }
                else {
                    seekBar.setProgress(0);
                }
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }
}

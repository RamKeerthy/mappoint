package com.ramkeerthy.locationalerter;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks, OnUserEarnedRewardListener,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static double MAX_VALUE = 9999;
    public static String COORDINATES = "COORDINATES";
    public static String IS_VIBRATION_ON = "IS VIBRATION ON";
    public static String RADIUS = "RADIUS";
    public static Boolean isStartLocationButtonEnabled = true;

    private RewardedInterstitialAd rewardedInterstitialAd;
    private String TAG = "MainActivity";

    private Location location;
    private TextView locationTv;
    private TextView destinationPin;
    private ImageButton pinButton;
    private Button startLocationAlertButton;
    private Button stopLocationAlertButton;
    private EditText radiusEditText;
    private CheckBox vibrateCheckbox;
    private AdView mAdView;

    private double currentLatitude;
    private double currentLongitude;
    private double destinationLatitude = MAX_VALUE;
    private double destinationLongitude = MAX_VALUE;
    private boolean isVibrationSelected = true;
    private double radius = 0.0;

    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    private FusedLocationProviderClient fusedLocationClient;

    // lists for permissions
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                loadAd();
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        locationTv = findViewById(R.id.location);
        pinButton = (ImageButton) findViewById(R.id.pin_button);
        destinationPin = (TextView) findViewById(R.id.destination_pin);
        startLocationAlertButton = (Button) findViewById(R.id.startService);
        stopLocationAlertButton = (Button) findViewById(R.id.stopService);
        radiusEditText = (EditText) findViewById(R.id.radius);
        vibrateCheckbox = (CheckBox) findViewById(R.id.vibrate_checkbox);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        // we add permissions we need to request location of the users
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION);

        permissionsToRequest = permissionsToRequest(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(
                        new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }

        // we build google api client
        googleApiClient = new GoogleApiClient.Builder(this).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();

        pinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                CoordinatesParcelable coordinates = new CoordinatesParcelable(currentLatitude, currentLongitude);
                intent.putExtra("COORDINATES", coordinates);
                startActivityForResult(intent, 10);
            }
        });

        startLocationAlertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    radius = Double.parseDouble(radiusEditText.getText().toString());
                } catch (Exception e) {

                }
                if (destinationLatitude == MAX_VALUE || destinationLongitude == MAX_VALUE || radius <= 0.250) {
                    if (destinationLatitude == MAX_VALUE || destinationLongitude == MAX_VALUE) {
                        Toast.makeText(MainActivity.this, getResources().getText(R.string.please_select_destination), Toast.LENGTH_SHORT).show();
                    } else if (radius <= 0.250) {
                        Toast.makeText(MainActivity.this, getResources().getText(R.string.please_enter_radius_greater_than_0_25KM), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, getResources().getText(R.string.please_enter_radius), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    CoordinatesResolver.radius = radius;
                    startService(v);
                }
            }
        });

        stopLocationAlertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(v);
            }
        });

        vibrateCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isVibrationSelected = isChecked;
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }

    public void loadAd() {
        // Use the test ad unit ID to load an ad.
        RewardedInterstitialAd.load(MainActivity.this, "ca-app-pub-6633239420907455/1167503944",
                new AdRequest.Builder().build(), new RewardedInterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(RewardedInterstitialAd ad) {
                        rewardedInterstitialAd = ad;

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                        alertDialogBuilder.setMessage("Ad content viewing disclaimer")
                                .setPositiveButton("View Ad - Currently No Rewards", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // View Ad
                                        rewardedInterstitialAd.show(MainActivity.this, MainActivity.this);
                                        rewardedInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                                            /** Called when the ad failed to show full screen content. */
                                            @Override
                                            public void onAdFailedToShowFullScreenContent(AdError adError) {
                                                Log.i(TAG, "onAdFailedToShowFullScreenContent");
                                            }

                                            /** Called when ad showed the full screen content. */
                                            @Override
                                            public void onAdShowedFullScreenContent() {
                                                Log.i(TAG, "onAdShowedFullScreenContent");
                                            }

                                            /** Called when full screen content is dismissed. */
                                            @Override
                                            public void onAdDismissedFullScreenContent() {
                                                Log.i(TAG, "onAdDismissedFullScreenContent");
                                            }
                                        });
                                    }
                                });
                        // Create the AlertDialog object and return it
                        alertDialogBuilder.create();
                        alertDialogBuilder.show();

                        Log.e(TAG, "onAdLoaded");
                    }

                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        Log.e(TAG, "onAdFailedToLoad");
                    }
                });
    }

    public void startService(View view) {
        isStartLocationButtonEnabled = false;
        startLocationAlertButton.setEnabled(isStartLocationButtonEnabled);
        Intent serviceIntent = new Intent(MainActivity.this, LocationAlertService.class);
        CoordinatesParcelable coordinates = new CoordinatesParcelable(destinationLatitude, destinationLongitude);
        serviceIntent.putExtra(COORDINATES, coordinates);
        serviceIntent.putExtra(IS_VIBRATION_ON, isVibrationSelected);
        serviceIntent.putExtra(RADIUS, radius);

        startService(serviceIntent);
    }

    public void stopService(View view) {
        if (AlarmBroadcastReceiver.mp != null) {
            AlarmBroadcastReceiver.mp.stop();
        }

        if (AlarmBroadcastReceiver.vibrator != null) {
            AlarmBroadcastReceiver.vibrator.cancel();
        }
        Intent serviceIntent = new Intent(MainActivity.this, LocationAlertService.class);
        stopService(serviceIntent);
        isStartLocationButtonEnabled = true;
        startLocationAlertButton.setEnabled(isStartLocationButtonEnabled);
    }

    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        startLocationAlertButton.setEnabled(isStartLocationButtonEnabled);

        if (!checkPlayServices()) {
            locationTv.setText(getResources().getText(R.string.you_need_to_install_play_services));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        if (googleApiClient != null && googleApiClient.isConnected()) {
            fusedLocationClient.removeLocationUpdates(mLocationCallback);
            googleApiClient.disconnect();
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                CoordinatesResolver.currentLatitude = location.getLatitude();
                CoordinatesResolver.currentLongitude = location.getLongitude();
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
            }
        }

        ;

    };

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            locationTv.setText(getResources().getText(R.string.latitude) + " : " + location.getLatitude() + "\n" + getResources().getText(R.string.longitude) + " : " + location.getLongitude());
                            CoordinatesResolver.currentLatitude = location.getLatitude();
                            CoordinatesResolver.currentLongitude = location.getLongitude();
                            currentLatitude = location.getLatitude();
                            currentLongitude = location.getLongitude();
                        }
                    }
                });

        startLocationUpdates();
    }

    private void startLocationUpdates() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, getResources().getText(R.string.you_need_to_enable_permission), Toast.LENGTH_SHORT).show();
        }

        // LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            locationTv.setText(getResources().getText(R.string.latitude) + " : " + location.getLatitude() + "\n" + getResources().getText(R.string.longitude) + " : " + location.getLongitude());
            CoordinatesResolver.currentLatitude = location.getLatitude();
            CoordinatesResolver.currentLongitude = location.getLongitude();
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        permissionsRejected.add(perm);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            new AlertDialog.Builder(MainActivity.this).
                                    setMessage("These permissions are mandatory to get your location on background. You need to allow them all the time.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.
                                                        toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }
                } else {
                    if (googleApiClient != null) {
                        googleApiClient.connect();
                    }
                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == 10 || requestCode == Activity.RESULT_OK) {
                // static
                if (!CoordinatesResolver.destinationName.equals("")) {
                    destinationPin.setText(CoordinatesResolver.destinationName);
                }
                CoordinatesParcelable coordinatesParcelable = (CoordinatesParcelable) data.getParcelableExtra("COORDINATES");

                if (coordinatesParcelable != null) {
                    destinationLatitude = coordinatesParcelable.getLatitude();
                    destinationLongitude = coordinatesParcelable.getLongitude();
                }
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, getResources().getText(R.string.please_select_destination), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
        Log.i(TAG, "onUserEarnedReward");
        // TODO: Reward the user!
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}



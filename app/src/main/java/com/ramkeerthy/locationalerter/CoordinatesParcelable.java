package com.ramkeerthy.locationalerter;

import android.os.Parcel;
import android.os.Parcelable;

public class CoordinatesParcelable implements Parcelable {
    private double latitude;
    private double longitude;
    private String name;

    protected CoordinatesParcelable(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public CoordinatesParcelable(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static final Creator<CoordinatesParcelable> CREATOR = new Creator<CoordinatesParcelable>() {
        @Override
        public CoordinatesParcelable createFromParcel(Parcel source) {
            return new CoordinatesParcelable(source);
        }

        @Override
        public CoordinatesParcelable[] newArray(int size) {
            return new CoordinatesParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}

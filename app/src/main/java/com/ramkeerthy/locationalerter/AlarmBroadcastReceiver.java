package com.ramkeerthy.locationalerter;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import androidx.core.app.NotificationCompat;

public class AlarmBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "AlarmBroadcastReceiver";
    public static final String STOP_CHANNEL_ID = "Location Alert";
    public static final int ALARM_NOTIFICATION_ID = 2;

    public static MediaPlayer mp;
    public static Vibrator vibrator;

    private boolean isVibrationEnabled = false;

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent serviceIntent = new Intent(context, LocationAlertService.class);
        context.stopService(serviceIntent);

        showStopNotification(context);

        long[] mVibratePattern = new long[]{0, 400, 400, 400, 400, 400, 400, 400};
        final int[] mAmplitudes = new int[]{0, 128, 0, 128, 0, 128, 0, 128};

        isVibrationEnabled = intent.getExtras().getBoolean(LocationAlertService.IS_VIBRATE);

        mp = MediaPlayer.create(context, R.raw.ring1);
        mp.setLooping(true);
        mp.start();

        if(isVibrationEnabled) {
            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createWaveform(mVibratePattern, mAmplitudes, 0));
            } else {
                //deprecated in API 26
                vibrator.vibrate(mVibratePattern, 3);
            }
        }

    }

    public void showStopNotification(Context context) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            NotificationChannel stopServiceChannel = new NotificationChannel(
                    STOP_CHANNEL_ID,
                    "Location Alert Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(stopServiceChannel);

            Intent wakeupIntent = new Intent(context, WakeUpActivity.class);
            PendingIntent wakeupPendingIntent = PendingIntent.getActivity(context, 0, wakeupIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, STOP_CHANNEL_ID)
                    .setSmallIcon(R.drawable.location_alerter)
                    .setContentTitle(context.getResources().getText(R.string.stop_location_alert))
                    .setContentText(context.getResources().getText(R.string.click_to_stop_activity))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setFullScreenIntent(wakeupPendingIntent, true);

            Notification alarmNotification = notificationBuilder.build();

            notificationManager.notify(ALARM_NOTIFICATION_ID, alarmNotification);
        }
        else {
            Intent wakeIntent = new Intent(context, WakeUpActivity.class);
            wakeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(wakeIntent);
        }
    }


}

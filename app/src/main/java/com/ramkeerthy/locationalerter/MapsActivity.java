package com.ramkeerthy.locationalerter;

import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    final int AUTOCOMPLETE_REQUEST_CODE = 1;
    private double latitude;
    private double longitude;

    EditText searchEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Button okButton = (Button) findViewById(R.id.ok_button);

        Intent intent = getIntent();
        CoordinatesParcelable coordinatesParcelable = (CoordinatesParcelable) intent.getParcelableExtra("COORDINATES");

        if(coordinatesParcelable != null) {
            latitude = coordinatesParcelable.getLatitude();
            longitude = coordinatesParcelable.getLongitude();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        searchEditText = (EditText) findViewById(R.id.search_edittext);

        // Initialize place API
        if (!Places.isInitialized()) {
            Places.initialize(this, getResources().getString(R.string.google_maps_key));
        }

        searchEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(MapsActivity.this);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                CoordinatesParcelable coordinates = new CoordinatesParcelable(latitude, longitude);
                resultIntent.putExtra("COORDINATES", coordinates);
                setResult(MapsActivity.RESULT_OK, resultIntent);
                finish();
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng currentCoordinates = new LatLng(latitude, longitude);

        //  LatLng myLocation = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
        mMap.addMarker(new MarkerOptions()
                .position(currentCoordinates)
                .draggable(true));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentCoordinates, 15.0f));

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker arg0) {
                // TODO Auto-generated method stub
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onMarkerDragEnd(Marker arg0) {
                // TODO Auto-generated method stub

                // pin location

                LatLng currentPosition = new LatLng(arg0.getPosition().latitude, arg0.getPosition().longitude);
                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(currentPosition)
                        .draggable(true));
                // mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(arg0.getPosition(), 15.0f));


            }

            @Override
            public void onMarkerDrag(Marker arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Place place = Autocomplete.getPlaceFromIntent(data);
                searchEditText.setText(place.getName());

                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(place.getLatLng())
                        .draggable(true)
                        .title(place.getName()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15.0f));

                // static
                CoordinatesResolver.destinationLatitude = place.getLatLng().latitude;
                CoordinatesResolver.destinationLongitude = place.getLatLng().longitude;
                CoordinatesResolver.destinationName = place.getName();

                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
